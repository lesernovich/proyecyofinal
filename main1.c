
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "alumnos.h"

int main(int argc, const char * argv[]) {
    int opcion=0;
    int cantidad=0; // para usar en el case 1, cantidad de alumnos a crear
    int j=0; // uso general de los for
    do {
        opcion=menu();
        switch (opcion) {
            case 1:
                 printf("Crear una nueva lista de alumnos\n");
                 printf("¿Cuantos alumnos quieres crear?:");
                 scanf("%d",&cantidad);
                 for(j=0;j<cantidad;j++){
                   printf("Capturando datos del alumno %d\n",j+1);
                   nuevoAlumno();
                 }
                break;
            case 2:
                printf("Guardar lista al archivo\n");
                grabaRegistros(listaAlumnos,indiceAlArreglo);
                break;
            case 3:
                printf("Leer la lista desde el archivo\n");
                indiceAlArreglo=registrosEnArchivo();
                leerRegistros(indiceAlArreglo);
                break;
            case 4:
                printf("Mostrar todos los datos de la lista\n");
                imprimirLista();
                break;
            case 5:
                printf("Agregar un nuevo alumno a la lista\n");
                nuevoAlumno();
                break;
            case 6:
                printf("Promedio del grupo\n");
                promedio();
                break;
            case 7:
                printf("Busqueda por nombre\n");
                buscarNombre();
                break;
            case 8:
                printf("Busqueda por edad\n");
                buscarEdad();
                break;
            case 9:
                printf("Eliminacion de alumnos\n");
            eliminarAlumno();
                break;
            case 10:
                printf("Editor de datos\n");
                Modificar();
                break;
            case 0:
                printf("Desarrollado por Bhx bajo licencia de LeserCORPS TM.\n");
                break;
            default:
                printf("Opcion no válida\n");
                break;
        }

    } while (opcion != 0);
    return 0;
}
