

#define MAX 180
#ifndef alumnos_h
#define alumnos_h
struct alumnos
{
    int edad;
    char nombre[120];
    char genero;
    char carrera[50];
    char nCuenta[10];
    float promedio;
};
typedef struct alumnos ALUMNO;

int indiceAlArreglo=0; // esta variable global se usar· para almacenar el indice del ultimo alumno ingresado al arreglo.
ALUMNO listaAlumnos[MAX]; // declaraciÛn de un arreglo de alumnos de 180 elementos



void insertarAlumno(ALUMNO al){
    if (indiceAlArreglo >= 0 && indiceAlArreglo < MAX) { // verificamos que inidice este en los rangos correctos
        listaAlumnos[indiceAlArreglo]=al;
        indiceAlArreglo++;
    }else{
        printf("El indice apunta fuera del arreglo, favor de revisar la logica");
    }
}

ALUMNO nuevoAlumno(){
    ALUMNO tmp;
    printf("Introduce la edad:");
    scanf("%d",&tmp.edad);
    printf("Introduce el genero [M o F]:");
    scanf(" %c",&tmp.genero);   // El espacio antes del %c es para que ignore espacios en blanco
    printf("Introduce el nombre:");
    scanf("%*c%[^\n]",tmp.nombre);
    printf("Introduce Carrera:");
    scanf("%*c%[^\n]",tmp.carrera);
    printf("Introduce numero de cuenta:");
    scanf("%*c%[^\n]",tmp.nCuenta);
    printf("Introduce tu promedio:");
    scanf("%f",&tmp.promedio);

    insertarAlumno(tmp);
    return tmp;
}

void imprimeAlumno(ALUMNO alu){
    printf("\tNombre:%s\n",alu.nombre);
    printf("\tEdad:%d\n",alu.edad);
    printf("\tGenero:%c\n",alu.genero);
    printf("\tCarrera:%s\n",alu.carrera);
    printf("\tNumero de Cuenta:%s\n",alu.nCuenta);
    printf("\tPromedio:%.2f\n",alu.promedio);
    printf("+---------------------------------+\n\n");
}


void imprimirLista(){
    int j=0;
    for (j = 0; j < indiceAlArreglo; j++) {
        printf("+--------- # de lista: %d ---------+*\n",j+1);
        imprimeAlumno(listaAlumnos[j]);
    }
}



int menu(){
    int op=0;
    printf("\n--------------------- Menu -------------------\n");
    printf("(1) Crear lista.\n");
    printf("(2) Guardar a archivo.\n");
    printf("(3) Leer desde archivo.\n");
    printf("(4) Mostrar lista.\n");
    printf("(5) Agregar alumno. \n");
    printf("(6) Obtener promedio de alumnos.\n");
    printf("(7) Buscar alumno por nombre. \n");
    printf("(8) Buscar alumno por edad. \n");
    printf("(9) Eliminar alumno(por numero de lista). \n");
    printf("(10) Modificar datos (nombre) \n");
    printf("(0) SALIR\n");
    printf("\n\nElige una opcion:");
    scanf("%d",&op);

    return op;
}

void grabaRegistros(ALUMNO r[], int tam){
    FILE *ptrF;

    if((ptrF=fopen("Evaluacion.dat","w"))==NULL){
        printf("el archivo no se puede abrir\n");
    }else{
        fwrite(r,sizeof(ALUMNO),tam,ptrF);
    }

    fclose(ptrF);
}

void leerRegistros(int tam){

    FILE *ptrF;

    if((ptrF=fopen("Evaluacion.dat","rb"))==NULL){
        printf("el archivo no se puede abrir\n");
    }
    else{
        //for /*(int i=0;i<tam;i++)*/
        fread(listaAlumnos,sizeof(ALUMNO),tam,ptrF);
    }

    fclose(ptrF);
}

int registrosEnArchivo(){
    FILE *ptrF;
    int contador=0;
    ALUMNO  basura;
    if((ptrF=fopen("Evaluacion.dat","rb"))==NULL){
        printf("el archivo no se puede abrir\n");
    }
    else{
        while(!feof(ptrF)){
            if (fread(&basura,sizeof(ALUMNO),1,ptrF))
                contador++;
        }

    }
    fclose(ptrF);
    return contador;
}
void buscarNombre(){
  _Bool encontrado=false;
  int i=0;
  char nombre[120];
  printf("¿A quien deseas buscar? \n\n");
  scanf("%s",nombre);
  for (i = 0; i <MAX; i++) {
      if (strcmp(nombre,listaAlumnos[i].nombre)==0) {
        encontrado=true;
        printf("Hey, mira! he encontrado esto:\n\n");
        printf("Nombre:%s\n",listaAlumnos[i].nombre);
        printf("Edad:%d\n",listaAlumnos[i].edad);
        printf("Genero:%c\n",listaAlumnos[i].genero);
        printf("Carrera:%s\n",listaAlumnos[i].carrera);
        printf("Numero de cuenta:%s\n",listaAlumnos[i].nCuenta);
        printf("Promedio:%.2f\n",listaAlumnos[i].promedio);
      }
  }
  if(encontrado==false) {
    printf("No se encontraron alumnos con ese nombre\n");
}
}
void Modificar() {
    _Bool encontrado=false;
  int i,temporal;
  char nombre[120];
  printf("¿A quien deseas editar(introduce nombre)? \n\n");
  scanf("%s",nombre);
  for (i = 0; i <MAX; i++) {
      if (strcmp(nombre,listaAlumnos[i].nombre)==0) {
        encontrado=true;
        printf("Elige el campo a modificar: \n");
        printf("(1)Nombre\n");
        printf("(2)Edad\n");
        printf("(3)Genero\n");
        printf("(4)Carrera\n");
        printf("(5Numero de cuenta\n");
        printf("(6)Promedio\n");
        scanf("%d",&temporal);
        switch (temporal) {
          case 1: printf("La nombre actual es:%s\n",listaAlumnos[i].nombre);
          printf("Introduce el nuevo nombre: \n");
          scanf("%*c%[^\n]",listaAlumnos[i].nombre);
          break;
          case 2: printf("La edad actual es:%d\n",listaAlumnos[i].edad);
          printf("Introduce la nueva edad: ");
          scanf("%d",&listaAlumnos[i].edad);
          break;
          case 3: printf("El genero actual es:%c\n",listaAlumnos[i].genero);
          printf("Introduce el nuevo genero: \n");
          scanf(" %c",&listaAlumnos[i].genero);
          break;
          case 4: printf("La carrera actual es:%s\n",listaAlumnos[i].carrera);
          printf("Introduce la nueva carrera: \n");
          scanf("%s",listaAlumnos[i].carrera);
          break;
          case 5: printf("El numero de cuenta actual es:%s\n",listaAlumnos[i].nCuenta);
          printf("Introduce el nuevo numero de cuenta: \n");
          scanf("%s",listaAlumnos[i].nCuenta);
          break;
          case 6: printf("El promedio actual es:%f\n",listaAlumnos[i].promedio);
          printf("Introduce el nuevo promedio: \n");
          scanf("%f",&listaAlumnos[i].promedio);
          break;
          default:
          printf("Opcion no valida\n");
          break;
        }
      }
    }
        if(encontrado==false) {
          printf("No se encontraron alumnos con ese nombre\n");
        }

}
void promedio() {
  float temporal=0.0;
  int divisor=0, i=0;
  for (i = 0; i <MAX; i++) {
  temporal+=listaAlumnos[i].promedio;
  if (listaAlumnos[i].promedio>0) {
    divisor++;
  }
      }
    printf("El promedio es: %.2f\n",temporal/divisor);
}
void buscarEdad(){
  _Bool encontrado=false;
  int i=0, edad=0;
  printf("¿Introduce la edad a buscar? \n\n");
  scanf("%d",&edad);
  for (i = 0; i <MAX; i++) {
      if (edad==listaAlumnos[i].edad) {
        encontrado=true;
        printf("Hey, mira! he encontrado esto:\n\n");
        printf("Nombre:%s\n",listaAlumnos[i].nombre);
        printf("Edad:%d\n",listaAlumnos[i].edad);
        printf("Genero:%c\n",listaAlumnos[i].genero);
        printf("Carrera:%s\n",listaAlumnos[i].carrera);
        printf("Numero de cuenta:%s\n",listaAlumnos[i].nCuenta);
        printf("Promedio:%.2f\n",listaAlumnos[i].promedio);
      }
  }
  if(encontrado==false) {
    printf("No se encontraron alumnos con esa edad\n");
}
}
void eliminarAlumno() {
  int i,temporal,alumno;
  printf("Numero de lista a eliminar\n");
  scanf("%d",&alumno);
  for (i = 0; i < MAX; i++) {
listaAlumnos[i]=listaAlumnos[i+1];
}
    indiceAlArreglo-=1;
  }
#endif
